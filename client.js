$(function () {
    
    var socket = io();

	if (typeof sessionStorage !== 'undefined' && sessionStorage){
        
        // if takeCallsHere is null on load, set it to false 
        if (sessionStorage.getItem("takeCallsHere") === null){
            sessionStorage.setItem("takeCallsHere", false);
        }

    }

    // Click handlers
    $(document).on('click', '.action__request-caller', function() {
        socket.emit('request caller', socket.id);
    });
    $(document).on('click', '.action--answer', function() {
        var callerid = $(this).parents('.caller').data('callerid');
        answerCall(callerid, true);
    });


    // Actions to take on socket events

    // On initial socket connection
    socket.on('connect', function () {
        $('title').text('Take Calls Here (' + socket.id + ')');

        // On initial connect, check if current tab can take calls
        socket.emit('can I take calls here', socket.id);

        // Set timer to assert take calls here control if no responses after a set amount of time
        takeCallsHereTimer = setTimeout(function() {
            socket.emit('take calls here', socket.id);
            $('.checkbox__take-calls-here').prop('checked', true).prop('disabled', true).trigger('change');
        }, 4*1000);

    });

    // Another tab is asserting control
    socket.on('take calls here', function(incomingID) {
        
        // If another tab has asserted control, give up control in this tab
        if (incomingID != socket.id){

            $('.checkbox__take-calls-here').prop('checked', false).prop('disabled', false).trigger('change');
            sessionStorage.setItem("takeCallsHere", false);

            // Another tab/instance has asserted they are taking calls, stop the take calls timer
            clearTimeout(takeCallsHereTimer);
            takeCallsHereTimer = '';
        }
    });

    // Another tab is asking if they can assert control of taking calls
    socket.on('can I take calls here', function(incomingID) {
        
        // If it wasn't this current tab that made the request and this tab is set to take calls, assert that to the room
        if (incomingID != socket.id && $('.checkbox__take-calls-here').prop('checked')){
            socket.emit('take calls here', socket.id);
        }
    });

    // A tab has been closed and a new tab has been selected to take calls
    socket.on('promoted', function(incomingID) {
        
        // If this tab has been selected, assert this to the room and update the checkbox
        if (incomingID == socket.id){
            socket.emit('take calls here', socket.id);
            $('.checkbox__take-calls-here').prop('checked', true).trigger('change');
            
            // Display alert to the user that this tab is now taking calls
            promoteModal.open();
        }
    });

    // Update the count of currently connected clients
    socket.on('clients', function(data) {
      updateClientCount(data.clients);
    });

    // Simulated incoming call, add them to the sidebar and 'Answer' the call in the correct tab
    socket.on('incoming call', function(data) {

        var person = new Person();
        person.number = data.phone;
        person.firstName = data.firstName;
        person.lastName = data.lastName;
        person.avatar = data.avatar;
        person.json = JSON.stringify(data);
        person.id = Math.random().toString(36).substr(2, 9);
        person.addToList();
        
        answerCall(person.id);
    });


    // Update take calls here status depending on state of checkbox
    $('.checkbox__take-calls-here').on('click', function() {
        if ($('.checkbox__take-calls-here').is(':checked')){
        
            // Assert control to the room
            socket.emit('take calls here', socket.id);
            
            // Store the value of the checkbox in session storage
            sessionStorage.setItem("takeCallsHere", true);
            
            // Disable the checkbox so the user can't stop taking calls from the active tab
            $(this).prop('disabled', true);
      
        }else{
            // Store the value of the checkbox in session storage
            sessionStorage.setItem("takeCallsHere", false);
        }
    });

    // Basics of an function to answer a selected call and display on the screen
    function answerCall(callerid, force){
        var caller = '';

        // Check to make sure this client is allow to take calls here
        if ($('.checkbox__take-calls-here').is(':checked') || force){
            
            // Loop through callers to find the new call
            $('.caller').each(function() {
                if ($(this).data('callerid') == callerid){
                    caller = $(this);
                }
            })

            // Grab a JSON blob of the caller data
            var callerjson = JSON.parse(caller.find('.int--json').text());

            $('.caller--avatar img').attr('src', callerjson.avatar);
            $('.caller--name').text(callerjson.firstName.capitalize() + ' ' + callerjson.lastName.capitalize());
            $('.caller--phone').text(callerjson.phone);
            
            $('.caller--default').hide();
            $('.caller--details').show();
        }
    }


    // Log all socket events (for debugging)
    socket.on("*",function(event,data) {
        console.log(event, data);
    });

  });

// Display the number of additional clients/tabs connected to the user
function updateClientCount(amount){
    if ($('.client--count').length){
        $('.client--count .number').text(amount);
    }else{
        var clientCountDiv = $('<div />', {class: 'client--count', html: '<span class="number">'+amount+'</span> clients connected'});
        $('body').append(clientCountDiv);
    }

    // Only show if there at least 2 clients connected
    if (amount < 2){
        $('.client--count').slideUp();
    }else{
        $('.client--count').slideDown();
    }
}


// Set up info modal for when a new tab/window is promoted automatically
var promoteModal = new tingle.modal({
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: "Close"
});

// set content
promoteModal.setContent('<h1>You are now taking calls in this window.</h1><p>The previous window or tab you were taking calls from was closed or lost connection.</p>');

// Extend String with a capitalize function
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

// Class for adding a caller to the sidebar call list
class Person {

    constructor(number, firstName, lastName, avatar, json, id) {
      this.number = number;
      this.firstName = firstName;
      this.lastName = lastName;
      this.avatar = avatar;
      this.json = json;
      this.id = id;
    }
    
    addToList() {
        var li = $('<li />', {class: 'caller', 'data-callerid': this.id });
        li.append( $('<img />', {class: 'int--photo', src: this.avatar}));
        li.append( $('<div />', {class: 'int--name', text: this.name() }));
        li.append( $('<div />', {class: 'int--phone', text: this.number}));
        li.append( $('<div />', {class: 'int--json', text: this.json}));
        li.append( $('<a />', {class: 'action--answer button', text: 'View', href: '#'}));
        li.hide();

        $('.list__interactions').append(li);
        li.slideDown();
    }

    name() {
        return this.firstName.capitalize() + ' ' + this.lastName.capitalize();
    }
  
}