const express = require('express');
const app = express();

var http = require('http').Server(app);
var io = require('socket.io')(http);
var request = require('request');
var clients = [];

// Routing
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

app.use('/assets', express.static('assets'));
app.use('/static', express.static('node_modules'));

app.get('/client.js', function(req, res){
    res.sendFile(__dirname + '/client.js');
});


io.sockets.on('connect', function(client) {

    // join the myqueue room
    client.join('myqueue');

    // Add the new client id to the clients object for tracking
    clients.push(client.id);

    // Send client count to clients
    var room = io.sockets.adapter.rooms['myqueue'];
    if (typeof room !== 'undefined'){
        io.to('myqueue').emit('clients', {clients: room.length, event: 'connect'});
    }

    client.on('disconnect', function() {

        // Remove client from clients object
        clients.splice(clients.indexOf(client.id), 1);

        // Send client count to clients
        var room = io.sockets.adapter.rooms['myqueue'];
        if (typeof room !== 'undefined'){
            io.to('myqueue').emit('clients', {clients: room.length, event: 'connect'});
        }

        // Primary client has disconnected, we need to pick a replacement
        if (clients.primary == client.id){
            client.to('myqueue').emit('promoted', clients[0]);
        }
    });

    // Relay tab assertation to take calls
    client.on('take calls here', function(msg){
        clients['primary'] = client.id;
        client.to('myqueue').emit('take calls here', client.id);
    });

    // Relay tab asking permission to take calls
    client.on('can I take calls here', function(msg){
        client.to('myqueue').emit('can I take calls here', client.id);
    });

    // Grab some random / fake user data for display
    client.on('request caller', function(msg){

        request('https://randomuser.me/api/', { json: true }, (err, res, body) => {
            if (err) { return console.log(err); }
        
            var samplePerson = {};
            samplePerson['firstName'] = res.body.results[0].name.first;
            samplePerson['lastName'] = res.body.results[0].name.last;
            samplePerson['phone'] = res.body.results[0].phone;
            samplePerson['avatar'] = res.body.results[0].picture.large;
        
            // Send the new person to the room
            io.to('myqueue').emit('incoming call', samplePerson);
                
          });
        
    });

});

// Listen for connections
http.listen(3000, function(){
  console.log('listening on *:3000');
});